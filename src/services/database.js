// @flow
import idb from 'idb';
import type { Meal } from '../types/meals';
import v4 from 'uuid';

export default class Database {
  connect = () => {
    return idb.open('calories', 1, upgradeDb => {
      if (!upgradeDb.objectStoreNames.contains('calories')) {
        const store = upgradeDb.createObjectStore('calories', {
          keyPath: 'id'
        });
        store.createIndex('id', 'id', { unique: true });
        store.createIndex('name', 'name', { unique: false });
        store.createIndex('calories', 'calories', {
          unique: false
        });
      }
    });
  };

  addMeal = ({
    name,
    calories
  }: {
    name: string,
    calories: number
  }): Promise<Meal> => {
    const newMeal = {
      id: v4(),
      name,
      calories
    };

    return this.connect()
      .then(db => {
        const tx = db.transaction('calories', 'readwrite');
        const store = tx.objectStore('calories');

        return store.add(newMeal);
      })
      .then(() => {
        return newMeal;
      })
      .catch(err => err);
  };

  updateMeal = ({ id, name, calories }: Meal): Promise<Meal> => {
    return this.connect()
      .then(db => {
        const tx = db.transaction('calories', 'readwrite');
        const store = tx.objectStore('calories');
        store.put({
          id,
          name,
          calories
        });
        return tx.complete;
      })
      .then(() => {
        return {
          id,
          name,
          calories
        };
      })
      .catch(err => err);
  };

  fetchMeals = () => {
    return this.connect()
      .then(db => {
        const tx = db.transaction('calories', 'readonly');
        const store = tx.objectStore('calories');
        return store.getAll();
      })
      .catch(err => err);
  };

  deleteMeal = (id: string) => {
    return this.connect()
      .then(db => {
        const tx = db.transaction('calories', 'readwrite');
        const store = tx.objectStore('calories');
        store.delete(id);
        return tx.complete;
      })
      .then(() => {
        return id;
      })
      .catch(err => err);
  };

  deleteAllMeals = () => {
    return this.connect()
      .then(db => {
        const tx = db.transaction('calories', 'readwrite');
        const store = tx.objectStore('calories');
        store.clear();
        return tx.complete;
      })
      .catch(err => err);
  };
}

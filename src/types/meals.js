// @flow
export type Meal = {|
  +id: string,
  +name: string,
  +calories: number
|};

export type Meals = Array<Meal>;

export type AddMealItem = {|
  type: 'ADD_MEAL_SUCCESS',
  meal: Meal
|};

export type FetchMeals = {|
  type: 'FETCH_MEALS_SUCCESS',
  meal: Meals
|};

export type UpdateMeal = {|
  type: 'UPDATE_MEAL_SUCCESS',
  meal: Meal
|};

export type SelectToEdit = {|
  type: 'SELECT_TO_EDIT',
  id: string
|};

export type CancelEdit = {
  type: 'CANCEL_EDIT'
};

export type DeleteMeal = {
  type: 'DELETE_MEAL',
  id: string
};

export type DeleteAllMeals = {
  type: 'DELETE_ALL_MEALS'
};

export type Actions =
  | AddMealItem
  | FetchMeals
  | UpdateMeal
  | SelectToEdit
  | CancelEdit
  | DeleteMeal
  | DeleteAllMeals;

export type State = {
  +meals: {
    +allMeals: Array<Meal>,
    +currentlyEdited: string
  }
};

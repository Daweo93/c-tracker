// @flow
import type { Actions as MealsActions, State as MealsState } from './meals';

export type Action = MealsActions;
export type Store = MealsState;
export type GetState = () => Store;
export type Dispatch = (
  action: Action | ThunkAction | PromiseAction | Array<Action>
) => any;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
export type PromiseAction = Promise<Action>;

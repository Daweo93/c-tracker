// @flow
import type { Dispatch } from '../types';
import Database from '../services/database';
import type { Actions, Meal } from '../types/meals';

const database = new Database();

export const addMealItem = (meal: { name: string, calories: number }) => (
  dispatch: Dispatch
) => {
  return database.addMeal(meal).then(meal => {
    dispatch({
      type: 'ADD_MEAL_SUCCESS',
      meal
    });
  });
};

export const fetchMeals = () => (dispatch: Dispatch) => {
  return database.fetchMeals().then(meal => {
    dispatch({
      type: 'FETCH_MEALS_SUCCESS',
      meal
    });
  });
};

export const updateMealItem = (meal: Meal) => (dispatch: Dispatch) => {
  return database
    .updateMeal(meal)
    .then(meal => {
      dispatch({
        type: 'UPDATE_MEAL_SUCCESS',
        meal
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const selectToEdit = (id: string): Actions => ({
  type: 'SELECT_TO_EDIT',
  id
});

export const cancelEdit = (): Actions => ({
  type: 'CANCEL_EDIT'
});

export const deleteMealItem = (id: string) => (dispatch: Dispatch) => {
  return database.deleteMeal(id).then(id => {
    dispatch({
      type: 'DELETE_MEAL',
      id
    });
  });
};

export const deleteAllMeals = () => (dispatch: Dispatch) => {
  return database.deleteAllMeals().then(() => {
    dispatch({
      type: 'DELETE_ALL_MEALS'
    });
  });
};

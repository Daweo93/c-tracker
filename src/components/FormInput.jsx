// @flow
import React from 'react';

type Props = {
  label: string,
  placeholder: string,
  name: string,
  inputChanged: (SyntheticInputEvent<HTMLInputElement>) => void,
  value: string | number,
  type: string,
  error: string
};

const FormInput = ({
  label,
  placeholder,
  name,
  inputChanged,
  value,
  type,
  error
}: Props) => {
  return (
    <React.Fragment>
      <label className="uk-form-label">
        {label}
        <input
          className={'uk-input ' + (error ? 'uk-form-danger' : '')}
          placeholder={placeholder}
          name={name}
          value={value}
          type={type}
          onInput={(e: SyntheticInputEvent<HTMLInputElement>) =>
            inputChanged(e)
          }
        />
      </label>
      {error && (
        <div className="uk-margin-small-top uk-text-danger uk-text-small">
          {label + ' ' + error}
        </div>
      )}
    </React.Fragment>
  );
};

export default FormInput;

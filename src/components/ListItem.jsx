// @flow
import React from 'react';
import type { Meal } from '../types/meals';

type Props = {|
  meal: Meal,
  selectToEditClicked: (id: string) => void,
  onDeleteClick: (id: string) => void
|};

const ListItem = ({ meal, selectToEditClicked, onDeleteClick }: Props) => {
  return (
    <li className="uk-flex uk-flex-middle">
      <p className="uk-margin-remove">
        <strong>{meal.name}</strong> <span>{meal.calories}</span>
      </p>
      <button
        className="uk-button uk-margin-auto-left js-edit"
        onClick={() => selectToEditClicked(meal.id)}
      >
        Edit
      </button>
      <button
        className="uk-button uk-button-danger uk-margin-small-left js-delete"
        onClick={() => onDeleteClick(meal.id)}
      >
        X
      </button>
    </li>
  );
};

export { ListItem };

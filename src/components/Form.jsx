// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addMealItem, cancelEdit, updateMealItem } from '../actions';
import { getCurrentlyEdited } from '../reducers/meals';
import type { Store } from '../types';
import type { Meal } from '../types/meals';
import FormInput from './FormInput';

type Props = {
  addMealItem: ({ name: string, calories: number }) => any,
  cancelEdit: () => any,
  updateMealItem: (meal: Meal) => any,
  mealToEdit?: Meal
};

type State = {
  id: string,
  calories: number,
  name: string,
  errors: Object
};

class Form extends Component<Props, State> {
  state = {
    id: '',
    calories: 0,
    name: '',
    errors: {}
  };

  inputs = ['name', 'calories'];

  componentDidUpdate(prevProps: Props) {
    if (
      this.props.mealToEdit !== prevProps.mealToEdit &&
      this.props.mealToEdit
    ) {
      const { id, name, calories } = this.props.mealToEdit;

      this.setState(() => ({
        id,
        name,
        calories
      }));
    }
  }

  validateInput = (name: string) => {
    if (this.state[name] === '' || Number(this.state[name]) === 0) {
      this.setState(prevState => ({
        errors: { ...prevState.errors, [name]: `is required!` }
      }));
    } else {
      this.setState(prevState => {
        const newErrors = { ...prevState.errors };
        delete newErrors[name];

        return { errors: newErrors };
      });
    }
  };

  isEdit = () => {
    return !!this.props.mealToEdit;
  };

  handleInputChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    const {
      target: { name, value }
    } = e;
    this.setState({ [name]: value }, () => {
      this.validateInput(name);
    });
  };

  handleFormSubmit = async (e: Event) => {
    e.preventDefault();
    await this.inputs.forEach(input => {
      this.validateInput(input);
    });

    const { id, calories, name, errors } = this.state;
    const { addMealItem, updateMealItem } = this.props;

    if (Object.keys(errors).length) {
      return;
    }

    if (this.isEdit()) {
      updateMealItem({ id, name, calories: Number(calories) }).then(() => {
        this.clearFields();
      });
    } else {
      addMealItem({ name, calories: Number(calories) }).then(() => {
        this.clearFields();
      });
    }
  };

  clearFields = () => {
    this.setState(() => ({
      name: '',
      calories: 0
    }));
  };

  render() {
    const { cancelEdit } = this.props;

    return (
      <form className="uk-grid" onSubmit={this.handleFormSubmit}>
        <div className="uk-width-1-1">
          <h3>Add Meal</h3>
        </div>
        <div className="uk-width-1-2">
          <FormInput
            label="Meal"
            name="name"
            placeholder="Meal name"
            value={this.state.name}
            type="text"
            error={this.state.errors.name}
            inputChanged={this.handleInputChange}
          />
        </div>
        <div className="uk-width-1-2">
          <FormInput
            label="Calories"
            name="calories"
            placeholder="Calories count"
            value={this.state.calories}
            type="number"
            error={this.state.errors.calories}
            inputChanged={this.handleInputChange}
          />
        </div>
        <div className="uk-width-expand uk-margin-top">
          <button className="uk-button uk-button-primary" type="submit">
            {this.isEdit() ? 'Update' : 'Save'}
          </button>

          {this.isEdit() && (
            <button
              className="uk-button uk-button-primary uk-margin-small-left js-cancel"
              onClick={(e: Event) => {
                e.preventDefault();
                this.clearFields();
                cancelEdit();
              }}
            >
              Cancel
            </button>
          )}
        </div>
      </form>
    );
  }
}

const mapStateToProps = (state: Store) => ({
  mealToEdit: getCurrentlyEdited(state)
});

export { Form };
export default connect(mapStateToProps, {
  addMealItem,
  cancelEdit,
  updateMealItem
})(Form);

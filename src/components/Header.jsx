// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { deleteAllMeals } from '../actions';

type Props = {
  deleteAllMeals: () => any
};

const Header = ({ deleteAllMeals }: Props) => {
  return (
    <React.Fragment>
      <div className="uk-padding-small uk-background-primary uk-light uk-text-center uk-position-relative">
        <div className="uk-logo">CalorieTracker</div>
        <div className="th-clear-all">
          <button
            className="uk-button uk-button-danger"
            onClick={deleteAllMeals}
          >
            Clear All
          </button>
        </div>
      </div>
    </React.Fragment>
  );
};

export { Header };
export default connect(null, { deleteAllMeals })(Header);

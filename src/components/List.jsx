// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ListItem } from './ListItem';
import type { Meals } from '../types/meals';
import type { Store } from '../types';
import { fetchMeals, selectToEdit, deleteMealItem } from '../actions';
import { getAllMeals } from '../reducers/meals';

type Props = {
  meals: Meals,
  selectToEdit: (id: string) => any,
  fetchMeals: () => any,
  deleteMealItem: (is: string) => any
};

class List extends Component<Props> {
  componentDidMount() {
    this.props.fetchMeals();
  }

  render() {
    const { meals, selectToEdit, deleteMealItem } = this.props;

    if (!meals.length) {
      return <h3 className="uk-text-center">No meals added yet!</h3>;
    }

    return (
      <ul className="uk-list uk-list-striped js-list">
        {meals.map(meal => (
          <ListItem
            meal={meal}
            selectToEditClicked={selectToEdit}
            onDeleteClick={deleteMealItem}
            key={meal.id}
          />
        ))}
      </ul>
    );
  }
}

const mapStateToProps = (state: Store) => ({
  meals: getAllMeals(state)
});

export { List };
export default connect(mapStateToProps, {
  fetchMeals,
  selectToEdit,
  deleteMealItem
})(List);

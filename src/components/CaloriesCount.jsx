// @flow
import React from 'react';
import { connect } from 'react-redux';
import { getTotalCaloriesCount } from '../reducers/meals';
import type { Store } from '../types';

type Props = {
  calories: number
};

const CaloriesCount = ({ calories = 0 }: Props) => {
  return (
    <h2 className="js-total uk-text-center">
      Total calories <span>{calories}</span>
    </h2>
  );
};

const mapStateToProps = (state: Store) => ({
  calories: getTotalCaloriesCount(state)
});

export { CaloriesCount };
export default connect(mapStateToProps)(CaloriesCount);

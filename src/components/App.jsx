// @flow
import React, { Component } from 'react';
import Form from './Form';
import Header from './Header';
import CaloriesCount from './CaloriesCount';
import List from './List';

class App extends Component<{}> {
  render() {
    return (
      <div>
        <Header />
        <div className="uk-container uk-container-small uk-margin-top">
          <div className="uk-card uk-card-default uk-card-body">
            <Form />
          </div>
          <CaloriesCount />
          <div className="uk-card uk-card-body uk-card-default">
            <List />
          </div>
        </div>
      </div>
    );
  }
}

export default App;

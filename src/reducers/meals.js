// @flow
import type { Actions, DeleteMeal, Meals, UpdateMeal } from '../types/meals';
import { combineReducers } from 'redux';
import type { Store } from '../types';

const updateMeals = (state: Meals, action: UpdateMeal) => {
  return state.map(meal => (meal.id === action.meal.id ? action.meal : meal));
};

const removeMeal = (state: Meals, action: DeleteMeal) => {
  return state.filter(meal => meal.id !== action.id);
};

const allMeals = (state: Meals = [], action: Actions): Meals => {
  switch (action.type) {
    case 'ADD_MEAL_SUCCESS':
      return [
        ...state,
        {
          calories: action.meal.calories,
          id: action.meal.id,
          name: action.meal.name
        }
      ];
    case 'FETCH_MEALS_SUCCESS':
      return [...state, ...action.meal];
    case 'UPDATE_MEAL_SUCCESS':
      return updateMeals(state, action);
    case 'DELETE_MEAL':
      return removeMeal(state, action);
    case 'DELETE_ALL_MEALS':
      return [];
    default:
      return state;
  }
};

const currentlyEdited = (state: string = '', action: Actions) => {
  switch (action.type) {
    case 'SELECT_TO_EDIT':
      return action.id;
    case 'CANCEL_EDIT':
      return '';
    default:
      return state;
  }
};

const meals = combineReducers({
  allMeals,
  currentlyEdited
});

export default meals;

export const getAllMeals = (state: Store) => {
  return state.meals.allMeals;
};

export const getCurrentlyEdited = (state: Store) => {
  return state.meals.allMeals.find(
    meal => meal.id === state.meals.currentlyEdited
  );
};

export const getTotalCaloriesCount = (state: Store) => {
  return state.meals.allMeals.reduce((accumulator, meal) => {
    return accumulator + meal.calories;
  }, 0);
};

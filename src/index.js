import React from 'react';
import { render } from 'react-dom';
import 'uikit/dist/css/uikit.css';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import Root from './Root';

render(<Root />, document.getElementById('root'));
registerServiceWorker();

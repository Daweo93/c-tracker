// @flow
import React from 'react';
import { mount } from 'enzyme';
import { Form } from '../components/Form';

describe('Form', () => {
  let cmp;
  let props;

  const form = () => {
    if (!cmp) {
      cmp = mount(<Form {...props} />);
    }
    return cmp;
  };

  beforeEach(() => {
    props = {
      addMealItem: jest.fn(() => Promise.resolve({})),
      cancelEdit: jest.fn(),
      updateMealItem: jest.fn(() => Promise.resolve({})),
      mealToEdit: undefined
    };
    cmp = undefined;
  });

  afterEach(() => {
    props.addMealItem.mockClear();
  });

  describe('Form elements', () => {
    it('should contains two input', () => {
      expect(form().find('input').length).toEqual(2);
    });

    it('should update state during typing into Meal input', () => {
      form()
        .find('input[name="name"]')
        .simulate('input', {
          target: { value: 'Meal', name: 'name' }
        });
      expect(form().state('name')).toEqual('Meal');
    });

    it('should update state during typing into Calories input', () => {
      const mealInput = form().find('input[name="calories"]');
      mealInput.simulate('input', {
        target: { value: 10, name: 'calories' }
      });
      expect(form().state('calories')).toEqual(10);
    });
  });

  describe('Form add item', () => {
    it('mealToEdit prop should be undefined', () => {
      expect(form().prop('mealToEdit')).toBeUndefined();
    });

    it('should contain one button', () => {
      expect(form().find('.uk-button').length).toEqual(1);
    });

    it('shouldnt contain cancel button', () => {
      expect(
        form()
          .find('.uk-button.js-cancel')
          .exists()
      ).toEqual(false);
    });

    it('shouldnt submit with invalid data', () => {
      form().setState({ name: '', calories: '' });
      form()
        .find('form')
        .simulate('submit', { preventDefault: () => null });
      expect(props.addMealItem).not.toHaveBeenCalled();
    });

    it('should submit with data', async () => {
      form().setState({ name: 'Meal', calories: 10 });
      await form()
        .find('form')
        .simulate('submit', { preventDefault: () => null });

      expect(props.addMealItem).toHaveBeenCalledTimes(1);
      expect(props.addMealItem).toHaveBeenCalledWith({
        calories: 10,
        name: 'Meal'
      });
    });

    it('should clear state after submit', async () => {
      await form().setState({ name: 'Meal', calories: 10 });
      await form()
        .find('form')
        .simulate('submit', { preventDefault: () => null });

      await form().update();
      expect(form().state('name')).toEqual('');
    });
  });

  describe('Form edit item', () => {
    beforeEach(() => {
      props.mealToEdit = {
        name: 'Chicken',
        id: '0',
        calories: 10
      };
    });

    it('mealToEdit prop should be equal to props.mealToEdit', () => {
      expect(form().prop('mealToEdit')).toEqual(props.mealToEdit);
    });

    it('should contains two buttons', () => {
      expect(form().find('.uk-button').length).toEqual(2);
    });

    it('submit button should contains text "Save"', () => {
      expect(
        form()
          .find('.uk-button[type="submit"]')
          .text()
      ).toEqual('Update');
    });

    it('cancel button should dispatch cancel action', () => {
      form()
        .find('.uk-button.js-cancel')
        .simulate('click');
      expect(props.cancelEdit).toHaveBeenCalledTimes(1);
    });

    it('should call updateMealItem', async () => {
      await form().setProps({
        mealToEdit: { id: '1', name: 'Beef', calories: 30 }
      });
      await form().setState({ name: 'Meal', calories: 10 });
      await form()
        .find('form')
        .simulate('submit', { preventDefault: () => null });

      expect(form().prop('addMealItem')).not.toHaveBeenCalled();
      expect(form().prop('updateMealItem')).toHaveBeenCalledTimes(1);
      expect(form().prop('updateMealItem')).toHaveBeenCalledWith({
        id: '1',
        name: 'Meal',
        calories: 10
      });
    });
  });
});

// @flow
import * as React from 'react';
import { shallow } from 'enzyme';
import { ListItem } from '../components/ListItem';

describe('ListItem Component', () => {
  let cmp;
  let props;

  beforeEach(() => {
    props = {
      meal: {
        id: '0',
        name: 'Chicken',
        calories: 0
      },
      selectToEditClicked: jest.fn(),
      onDeleteClick: jest.fn()
    };

    cmp = shallow(<ListItem {...props} />);
  });

  it('should contain meal item text', () => {
    expect(cmp.find('strong').text()).toEqual(props.meal.name);
    expect(cmp.find('span').text()).toEqual(props.meal.calories.toString());
  });

  it('should handle edit button', () => {
    cmp.find('.uk-button.js-edit').simulate('click');
    expect(props.selectToEditClicked).toHaveBeenCalledTimes(1);
  });

  it('should handle edit button click and pass id as parameter', () => {
    cmp.find('.uk-button.js-edit').simulate('click');
    expect(props.selectToEditClicked).toHaveBeenCalledWith(props.meal.id);
  });

  it('should handle delete button click', () => {
    cmp.find('.uk-button.js-delete').simulate('click');
    expect(props.onDeleteClick).toHaveBeenCalledTimes(1);
  });
});

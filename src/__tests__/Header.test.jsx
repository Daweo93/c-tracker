// @flow
import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { Header } from '../components/Header';

describe('Header', () => {
  let cmp;
  let props;

  beforeEach(() => {
    props = {
      deleteAllMeals: jest.fn()
    };

    cmp = shallow(<Header {...props} />);
  });

  it('Should render without crashing', () => {
    expect(cmp);
  });

  it('should contain app name', () => {
    expect(cmp.find('.uk-logo').text()).toEqual('CalorieTracker');
  });

  it('should contain reset all button', () => {
    expect(cmp.find('.uk-button').exists()).toEqual(true);
  });

  it('should call function after click on button', () => {
    cmp.find('.uk-button').simulate('click');
    expect(props.deleteAllMeals).toHaveBeenCalledTimes(1);
  });
});

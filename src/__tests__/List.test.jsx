// @flow
import React from 'react';
import { shallow } from 'enzyme';
import { List } from '../components/List';

describe('List component', () => {
  let cmp;
  let props;

  const list = () => {
    if (!cmp) {
      cmp = shallow(<List {...props} />);
    }
    return cmp;
  };

  beforeEach(() => {
    props = {
      meals: [],
      selectToEdit: jest.fn(),
      fetchMeals: jest.fn()
    };

    cmp = undefined;
  });

  describe('Meal items present', () => {
    beforeEach(() => {
      props.meals = [
        {
          id: '0',
          name: 'Chicken',
          calories: 0
        },
        {
          id: '1',
          name: 'Beef',
          calories: 1000
        }
      ];
    });

    it('should contain two ListItem elements', () => {
      expect(list().find('ListItem').length).toEqual(2);
    });

    it('should have key props', () => {
      expect(
        list()
          .find('ListItem')
          .at(0)
          .key()
      ).toEqual('0');
      expect(
        list()
          .find('ListItem')
          .at(1)
          .key()
      ).toEqual('1');
    });
  });

  it('should show message when no meals added', () => {
    expect(
      list()
        .find('h3')
        .text()
    ).toEqual('No meals added yet!');
  });
});

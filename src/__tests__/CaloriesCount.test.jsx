// @flow
import React from 'react';
import { shallow } from 'enzyme';
import { CaloriesCount } from '../components/CaloriesCount';

describe('CaloriesCount', () => {
  let cmp;

  it('should show calories count', () => {
    cmp = shallow(<CaloriesCount calories={10} />);
    expect(cmp.find('span').text()).toEqual('10');
  });

  it('should show calories count', () => {
    cmp = shallow(<CaloriesCount />);
    expect(cmp.find('span').text()).toEqual('0');
  });
});
